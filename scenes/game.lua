-- Game, where the magic happens
local scene = {}
local sounds = require('lib/sounds')
local ui = {}

function scene.load()
    -- Set the scene
    scene.asteroid_count_timer = 30
    scene.gun_timer = 0.1
    scene.asteroid_count = 0
    scene.level = 0
    scene.score = 0
    scene.FILL_MODE = 'line'
    scene.countdown_timer = 5
    scene.asteroids = {}
    scene.bullets = {}
    scene.ship = {
        position = {x = 400, y = 300},
        speed = 0,
        acceleration = 300,
        rotation = 0,
        rotation_speed = 0.08,
        orientation = 0,
        direction = {x = 0, y = 0},
        velocity = {x = 0, y = 0}
    }

    -- Start the music
    love.audio.stop()
    sounds.music:setLooping(true)
    sounds.music:play()

    -- Initialize the background
    love.graphics.setBackgroundColor(COLORS.BLUE)
end

-- Update
function scene.update(dt)
    function generate_asteroid(radius)
        local positions = {
            {
                x = 64,
                y = math.random(love.graphics.getHeight())
            },
            {
                x = love.graphics.getWidth() - 64,
                y = math.random(love.graphics.getHeight())
            },
            {
                x = math.random(love.graphics.getWidth()),
                y = 64
            },
            {
                x = math.random(love.graphics.getWidth()),
                y = love.graphics.getHeight() - 64
            }
        }
        local orientations = {
            math.rad(0),
            math.rad(180),
            math.rad(90),
            math.rad(-90)
        }
        local posid = math.random(#positions)
        return {
            image = images.asteroid(radius),
            rotation = 0.01,
            spin = 0,
            speed = 1000,
            orientation = orientations[posid],
            position = positions[posid],
            velocity = {x = 0, y = 0},
            radius = radius
        }
    end

    function copy_asteroid(a)
        local b = generate_asteroid(math.floor(a.radius * math.random(5, 10) / 10))
        b.position = {x = a.position.x, y = a.position.y}
        return b
    end

    function asteroid_collision(object)
        for j, a in ipairs(scene.asteroids) do
            if
                (object.position.x < a.position.x + a.radius and object.position.x > a.position.x - a.radius and
                    object.position.y < a.position.y + a.radius and
                    object.position.y > a.position.y - a.radius)
             then
                return j
            end
        end
        return nil
    end

    local function calculate_movement(v, loop)
        v.velocity = {
            x = v.speed * (math.cos(v.orientation) * dt),
            y = v.speed * (math.sin(v.orientation) * dt)
        }
        v.position = {
            x = v.position.x + v.velocity.x * dt,
            y = v.position.y + v.velocity.y * dt
        }

        if loop then
            local radius = (v.radius or 32)
            if v.position.x > love.graphics.getWidth() + radius then
                v.position.x = 0
            end
            if v.position.x < 0 - radius then
                v.position.x = love.graphics.getWidth()
            end
            if v.position.y > love.graphics.getHeight() + radius then
                v.position.y = 0
            end
            if v.position.y < 0 - radius then
                v.position.y = love.graphics.getHeight()
            end
        end
    end

    -- In testing sometimes asteroid counts are off. This function counts the asteroids.
    if scene.asteroid_count_timer <= 0 then
        scene.asteroid_count = 0
        for k, v in pairs(scene.asteroids) do
            scene.asteroid_count = scene.asteroid_count + 1
        end
        scene.asteroid_count_timer = 30
    end

    -- Check if we have a level up
    if scene.asteroid_count <= 0 then
        scene.bullets = {}
        scene.level = scene.level + 1
        while scene.asteroid_count < scene.level do
            table.insert(scene.asteroids, generate_asteroid(128))
            scene.ship.position = {x = 400, y = 300}
            scene.asteroid_count = scene.asteroid_count + 1
        end
        CURRENT_SCENE = 'countdown'
    end

    -- Calculate asteroid movement
    for k, v in ipairs(scene.asteroids) do
        v.spin = v.spin + v.rotation
        calculate_movement(v, true)
    end

    -- Calculate bullets
    local destroy_bullets = {}
    local destroy_asteroids = {}
    for k, v in ipairs(scene.bullets) do
        calculate_movement(v, false)

        local j = asteroid_collision(v)
        if (j ~= nil) then
            scene.score = scene.score + 136 - scene.asteroids[j].radius

            table.insert(destroy_bullets, k)
            table.insert(destroy_asteroids, j)
        end

        if
            v.position.x <= 0 or v.position.x >= love.graphics.getWidth() or v.position.y <= 0 or
                v.position.y >= love.graphics.getHeight()
         then
            table.insert(destroy_bullets, k)
        end
    end

    -- Bullet cleanup.
    for i, v in ipairs(destroy_bullets) do
        table.remove(scene.bullets, v)
    end

    -- Asteroid cleanup.
    for i, v in ipairs(destroy_asteroids) do
        -- Play crumbling sound.
        sounds.rock:stop()
        sounds.rock:play()

        -- Remove the asteroid from memory.
        local a = table.remove(scene.asteroids, v)

        -- Workaround: Sometimes a is nil.
        if a == nil then
            break
        end
        scene.asteroid_count = scene.asteroid_count - 1

        -- Big asteroids will break in half.
        if (a.radius >= 32) then
            local b = copy_asteroid(a)
            table.insert(scene.asteroids, b)
            table.insert(scene.asteroids, copy_asteroid(b))
            scene.asteroid_count = scene.asteroid_count + 2
        end
    end

    -- Input handling
    if love.keyboard.isDown('left') or love.keyboard.isDown('a') then
        scene.ship.rotation = -scene.ship.rotation_speed
    end
    if love.keyboard.isDown('right') or love.keyboard.isDown('d') then
        scene.ship.rotation = scene.ship.rotation_speed
    end

    if love.keyboard.isDown('up') or love.keyboard.isDown('w') then
        scene.ship.speed = scene.ship.speed + scene.ship.acceleration
    end
    if love.keyboard.isDown('down') or love.keyboard.isDown('s') then
        scene.ship.speed = scene.ship.speed - scene.ship.acceleration
    end

    if love.keyboard.isDown('space') and scene.gun_timer <= 0 then
        if (#scene.bullets <= 5) then
            sounds.laser:stop()
            sounds.laser:play()

            table.insert(
                scene.bullets,
                {
                    position = {
                        x = scene.ship.position.x - 8,
                        y = scene.ship.position.y - 8
                    },
                    speed = scene.ship.speed + 40000,
                    orientation = scene.ship.orientation
                }
            )
            scene.gun_timer = 0.05
        end
    end

    -- Update the player's ship.
    scene.gun_timer = scene.gun_timer - dt
    scene.ship.orientation = scene.ship.orientation + scene.ship.rotation
    scene.ship.speed = scene.ship.speed * 0.99
    calculate_movement(scene.ship, true)

    -- Check if the player is dead.
    if asteroid_collision(scene.ship) then
        CURRENT_SCENE = 'gameover'
    end

    -- Update the UI
    ui.status = 'LEVEL:' .. scene.level .. '\nASTEROIDS: ' .. scene.asteroid_count
    ui.score = 'SCORE:' .. scene.score
end

function scene.keyreleased(key)
    if key == 'left' or key == 'a' then
        scene.ship.rotation = 0
    end
    if key == 'right' or key == 'd' then
        scene.ship.rotation = 0
    end
end

-- Draw
function scene.draw()
    -- Bullets
    for k, v in ipairs(scene.bullets) do
        love.graphics.setColor(COLORS.WHITE)
        love.graphics.draw(images.bullet(), v.position.x, v.position.y)
    end

    -- Asteroids
    for k, v in ipairs(scene.asteroids) do
        love.graphics.setColor(COLORS.GRAY)
        love.graphics.draw(v.image, v.position.x, v.position.y, v.spin, 1, 1, v.radius, v.radius)
    end

    -- Player's Ship
    love.graphics.setColor(COLORS.RED)
    love.graphics.draw(
        images.ship(),
        scene.ship.position.x,
        scene.ship.position.y,
        scene.ship.orientation,
        1,
        1,
        16,
        16
    )

    -- User Interface
    love.graphics.setColor(COLORS.PALE)
    love.graphics.printf(ui.status, 0, 0, love.graphics.getWidth(), 'right')
    love.graphics.printf(ui.score, 0, 0, love.graphics.getWidth(), 'left')
end

return scene
