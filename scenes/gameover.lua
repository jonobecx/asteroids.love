local scene = {}
local loadedscores
local scores = nil
local ui = {message = 'GAME OVER', list = ''}
local countdown = 5
local countdown_timer = countdown

function scene.load()
end

function scene.update(dt)
    if scores == nil and SCENES.game.score ~= nil then
        -- Load an existing one.
        if love.filesystem.getInfo('scores.dat') then
            scores = TSerial.unpack(love.filesystem.read('scores.dat'))
        else
            scores = {
                {'JAX', 1000000000},
                {'JAX', 100000000},
                {'JAX', 10000000},
                {'JAX', 1000000},
                {'JAX', 100000},
                {'JAX', 10000},
                {'JAX', 1000},
                {'JAX', 100},
                {'JAX', 10}
            }
        end

        local newscores = {}
        ui.list = ''
        -- Check for a new high score
        for i, score in ipairs(scores) do
            if i < 11 and (score[1] ~= nil and score[2] ~= nil) then
                if SCENES.game.score > score[2] and ui.message ~= 'GAME OVER - NEW HIGH SCORE' then
                    -- Write the update
                    ui.message = 'GAME OVER - NEW HIGH SCORE'
                    table.insert(newscores, {'YOU', SCENES.game.score})
                    ui.list = ui.list .. '\n>' .. 'YOU' .. '\t\t' .. SCENES.game.score .. '<'
                end
                ui.list = ui.list .. '\n' .. score[1] .. '\t\t' .. score[2]
                table.insert(newscores, score)
            end
        end
        table.sort(
            newscores,
            function(a, b)
                return a[2] > b[2]
            end
        )
        love.filesystem.write('scores.dat', TSerial.pack(newscores))
    end

    -- Countdown
    if countdown_timer > 0 then
        countdown_timer = countdown_timer - dt
    end
end

function scene.draw()
    love.graphics.setColor(COLORS.PALE)
    love.graphics.printf(ui.message .. '\n\n' .. ui.list, 0, 64, love.graphics.getWidth(), 'center')

    local continue_message = 'Prepare to Launch: ' .. math.floor(countdown_timer)
    if countdown_timer <= 0 then
        continue_message = 'Press Any Key to Continue...'
    else end


    love.graphics.printf(
        continue_message,
        0,
        love.graphics.getHeight() - 64,
        love.graphics.getWidth(),
        'center'
    )
end

function scene.keypressed()
    if countdown_timer <= 0 then
        countdown_timer = countdown
        CURRENT_SCENE = 'game'
        SCENES.game.load()
        scores = nil
    end
end

return scene
