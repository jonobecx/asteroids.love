local scene = {}
local countdown_timer = 5

function scene.load() end

function scene.update(dt) 
    if countdown_timer < 1 then
        countdown_timer = 5
        CURRENT_SCENE = 'game'
    end
    countdown_timer = countdown_timer - dt
end

function scene.draw()
    love.graphics.setColor(COLORS.PALE)
    love.graphics.printf(
        'LEVEL ' .. (SCENES.game.level or 0) .. '\n\nLaunching in ' .. math.floor(countdown_timer),
        0,
        love.graphics.getHeight() / 2,
        love.graphics.getWidth(),
        'center'
    )
end

return scene