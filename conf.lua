function love.conf(t)
    t.window.title = "asteroids.love"

    t.modules.joystick = false
    t.modules.physics = false
end