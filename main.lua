require('lib/options')
require('lib/tserial')

images = require('lib/images')

SCENES = {}
CURRENT_SCENE = nil

function love.load()
    images.load()
    SCENES = {
        game = require('scenes/game'),
        countdown = require('scenes/countdown'),
        gameover = require('scenes/gameover'),
    }

    SCENES.game.load()
    SCENES.countdown.load()
    SCENES.gameover.load()

    CURRENT_SCENE = 'game'
end

function love.keypressed(key)
    if SCENES[CURRENT_SCENE].keypressed ~= nil then SCENES[CURRENT_SCENE].keypressed(key) end
end

function love.keyreleased(key)
    if SCENES[CURRENT_SCENE].keyreleased ~= nil then SCENES[CURRENT_SCENE].keyreleased(key) end
end
function love.update(dt)
    SCENES[CURRENT_SCENE].update(dt)
end

function love.draw()
    SCENES[CURRENT_SCENE].draw()
end
