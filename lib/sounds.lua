-- Table of sounds
return {
    laser = love.audio.newSource("sound/laser.mp3", "static"),
    rock = love.audio.newSource("sound/rock.mp3", "static"),
    music = love.audio.newSource("sound/music.ogg", "static")
}

