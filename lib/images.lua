local images = {}
local cache = {}

local function createCanvas(x, y)
    local c = love.graphics.newCanvas(x, y)
    love.graphics.setCanvas(c)
    return c
end

function images.load()
    -- Setup the pen for drawing.
    love.graphics.setLineStyle('smooth')
    love.graphics.setLineWidth(2)
    love.graphics.setLineJoin('miter')
end

images.ship = function(size, vertices)
    if cache.ship == nil then
        cache.ship = createCanvas(32, 32)
        love.graphics.polygon(FILL_MODE, {8, 16, 0, 0, 32, 16, 0, 32})
    end
    love.graphics.setCanvas()
    return cache.ship
end

images.asteroid = function(radius)
    local points = {}
    local center_x = radius
    local center_y = radius

    local degrees = 0
    for i = 0, 10 do
        table.insert(points, center_x + math.cos(math.rad(degrees)) * math.random(radius / 2, radius))
        table.insert(points, center_y + math.sin(math.rad(degrees)) * math.random(radius / 2, radius))
        degrees = degrees + 36
    end

    local image = createCanvas(radius * 2, radius * 2)

    love.graphics.setColor(COLORS.GRAY)
    love.graphics.polygon(FILL_MODE, points)
    love.graphics.setCanvas()

    return image
end

images.bullet = function()
    if cache.bullet == nil then
        cache.bullet = createCanvas(16, 16)
        love.graphics.circle('fill', 8, 8, 4)
    end
    love.graphics.setCanvas()
    return cache.bullet
end

return images
