# Love 2D Asteroids

## Cloning the repository

Requires https://git-lfs.github.com/ installed to clone the music.

## Building

Using boon:

        boon build .
        boon build . --target windows
        boon build . --target macos

A docker image `cenetex/boondock` has been created with the necessary dependencies to launch boon from a CI process. 

## Acknowledgements

- Music by [PunchDeck](https://www.reddit.com/user/PunchDeck/)([Spotify](https://open.spotify.com/artist/7kdduxAVaFnbHJyNxl7FWV), [YouTube](https://www.youtube.com/c/PunchDeck)), used with permission under a [Creative Commons 3.0 License](https://creativecommons.org/licenses/by/3.0/).
- Sound Effects from [ZapSplat](https://www.zapsplat.com/music/roof-tile-drop-on-ground-and-smash/).